#include <bits/stdc++.h>

using namespace std;

long SixDigitRandom() {

    long i = 0;
    while (i < 99999 || i > 999999) {
        i = (rand() % 999999 + 1);
    }
    return i;
}

int getLastDigit(long n) {
    return n % 10;
}



int main () {

    srand(time(NULL));

    long iterasi = 100000;

    long n = 0;

    // variabel data untuk menyimpan jumlah frequensi masing-masing digit
    long data[10];
    // inisialiasi jumlah awal
    for (int i = 0; i < 10; i++) {
        data[i] = 0;
    }
    int idx;

    // cout << "Banyaknya iterasi: " << iterasi << endl;

    while (iterasi--) {

        n = SixDigitRandom();
        
        // cout << n << endl;

        while (n > 9) {
            idx = getLastDigit(n);
            n = n/10;
            data[idx]++;

            // cout << n << endl;
            // cout << idx << endl;
        }
        idx = n;
        data[idx]++;
    }

    // for (int i = 0; i < 10; i++) {
    //     cout << "freq " << i << ": " << data[i] << endl;
    // }

    //menyimpan ke file eksternal
    ofstream file;
    file.open("output.txt");
    for (int i = 0; i < 10; i++) {
        file << "freq: " << i << ": " << data[i] << endl;
    }
    file.close();

    return 0;
}