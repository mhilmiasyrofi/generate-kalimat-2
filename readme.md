## Milestone Generate Kalimat - Random 6 Digit OTP

### Deskripsi
Menentukan frequensi digit 0-9 dari bilangan random 6 digit. Bilangan random di generate selama 100000 kali. Contoh keluaran program ada di file `output.txt`

### Penjelasan
Generate random masih dengan library yang sudah ada

### Cara Penggunaan

```
g++ main.cpp -o main
```
